// base selector
const bs = `app-message`;

export const MESSAGE_DOM = {
  buttons: {
    viewMore: `${bs} .btn-view-more-message`,
    cancelMessage: `${bs} .btn-cancel-message`,
  },
  texts: {
    msgDetails: `${bs} .msg-details`,
  },
};
