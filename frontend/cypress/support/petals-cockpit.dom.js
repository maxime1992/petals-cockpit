// base selector
const bs = `app-header`;

export const PETALS_COCKPIT_DOM = {
  buttons: {
    goToAdminPage: `${bs} .btn-usr-admin`,
    userAvatar: `${bs} app-generate-icon`,
    logout: `${bs} .btn-logout-user`,
    logo: `${bs} .toolbar-logo`,
    toggleSidenavButton: `${bs} .sidenav-toggle`,
  },
};
