// base selector
const bs = `app-service-menu-view`;
const bsTree = `app-services-list`;

export const SERVICES_DOM = {};

export const SERVICES_TREE_DOM = {
  navTree: { navTreeServices: `${bsTree} .nav-list-tree` },
  texts: {
    servicesNames: `${bsTree} .item-name`,
  },
};
