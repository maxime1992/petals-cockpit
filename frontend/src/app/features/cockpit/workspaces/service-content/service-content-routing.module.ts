/**
 * Copyright (C) 2017-2018 Linagora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ResourceByIdGuard } from 'app/features/cockpit/workspaces/petals-content/resource-by-id.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'services',
  },
  {
    path: '',
    loadChildren:
      'app/features/cockpit/workspaces/service-content/service-view/service-view.module#ServiceViewModule',
  },
];

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', canActivateChild: [ResourceByIdGuard], children: routes },
    ]),
  ],
  providers: [ResourceByIdGuard],
  exports: [RouterModule],
})
export class ServiceContentRoutingModule {}
